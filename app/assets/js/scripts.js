/*!
 * fastshell
 * Fiercely quick and opinionated front-ends
 * https://HosseinKarami.github.io/fastshell
 * @author Hossein Karami
 * @version 1.0.5
 * Copyright 2018. MIT licensed.
 */
(function ($, window, document, undefined) {

  'use strict';
  var markerVectorLayer = null;

  function resetStats() {
    $('.overlay').show();
    $('.value').each(function () {
      $(this).text('-');
    });
  }

  function addMarker(map, coordinates) {
    if (markerVectorLayer) {
      map.removeLayer(markerVectorLayer);
    }

    var marker = new ol.Feature({
      geometry: new ol.geom.Point(
        ol.proj.fromLonLat([coordinates[0], coordinates[1]])
      ),
    });
    var myStyle = new ol.style.Style({
      image: new ol.style.Circle({
        radius: 8,
        fill: new ol.style.Fill({color: 'red'}),
        stroke: new ol.style.Stroke({
          color: [0, 0, 0], width: 1
        })
      })
    });
    marker.setStyle(myStyle);
    var vectorSource = new ol.source.Vector({features: [marker]});
    markerVectorLayer = new ol.layer.Vector({source: vectorSource,});

    map.addLayer(markerVectorLayer);
  }

  function animateCounters() {
    $('.value-number').each(function () {
      $(this).prop('Counter', 0).animate({
        Counter: $(this).text()
      }, {
        duration: 800,
        easing: 'swing',
        step: function (now) {
          $(this).text(Math.ceil(now));
        }
      });
    });
  }

  function requestStats(coordinates) {
    $.ajax('http://www.datasciencetoolkit.org/coordinates2statistics/' + coordinates[1] + ',' + coordinates[0], {
      success: function (res) {
        var data = res[0].statistics;
        console.log(data);
        Object.keys(data).forEach(function (key) {
          $('.' + key + ' > .desc').html(data[key].description);
          $('.' + key + ' > .value').html(data[key].value);
        });
        animateCounters();
        $('.overlay').hide();
      },
      dataType: 'jsonp',
      crossDomain: true
    });
  }

  function saveCookiesCoordinates(coordinates) {
    cookies.set('coordinate0', coordinates[0]);
    cookies.set('coordinate1', coordinates[1]);
  }

  function refreshStats(map, coordinates) {
    resetStats();
    addMarker(map, coordinates);
    requestStats(coordinates)
  }

  $(function () {

    var map = new ol.Map({
      target: 'map',
      layers: [
        new ol.layer.Tile({
          source: new ol.source.OSM()
        })
      ],
      view: new ol.View({
        center: ol.proj.fromLonLat([-73.999368, 40.729025]),
        zoom: 11,
        minZoom: 11,
        maxZoom: 11
      })
    });

    map.getInteractions().forEach(function (interaction, index, array) {
      map.removeInteraction(interaction);
    });

    if (cookies.get('coordinate0') && cookies.get('coordinate1')) {
      var coordinates = [cookies.get('coordinate0'), cookies.get('coordinate1')];
      console.log('Coordinates from cookies', coordinates);
      refreshStats(map, coordinates);
    }

    map.on('click', function (e) {
      // get openstreet map real world coordinates
      var coordinates = ol.proj.toLonLat(e.coordinate);
      saveCookiesCoordinates(coordinates);

      refreshStats(map, coordinates);
    });

  });
})(jQuery, window, document);
